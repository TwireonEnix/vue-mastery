const app = new Vue({
  data: () => ({
    product: 'Socks',
    description: 'You put these on your feet.',
    selectedVariant: 0,
    url: 'https://navcadar.com',
    onSale: true,
    details: ['80% cotton', '20% polyester', 'Gender-neutral'],
    variants: [
      {
        id: 2234,
        color: 'green',
        image: './assets/img/greenOnWhiteSocks.jpg',
        quantity: 50
      },
      {
        id: 2235,
        color: 'blue',
        image: './assets/img/vmSocks-blue-onWhite.jpg',
        quantity: 0
      }
    ],
    sizes: ['sm', 'md', 'lg', 'xl'],
    cart: 0,
    brand: 'Vue Mastery'
  }),
  created() {
    return setTimeout(function () {
      console.log('timeout ran');
      this.product = 'Blue socks';
    }, 2000);
  },
  /** Holy fucking molly, i'm having a stroke with this much mutation. Now I highly dislike this
   * I'll see if I can get a more functional approach with react.
   */
  methods: {
    addToCart() {
      this.cart += 1;
      this.variants[this.selectedVariant].quantity -= 1;
      const count = this.cart;
      document.title = `Clicked ${count} times`;
    },
    removeFromCart() {
      if (this.cart > 0) {
        this.cart -= 1;
        this.variants[this.selectedVariant].quantity += 1;
      }
    },
    updateVariant(index) {
      this.selectedVariant = index;
    }
  },
  computed: {
    inStock() {
      return !!this.variants[this.selectedVariant].quantity;
    },
    title() {
      return `${this.brand} ${this.product}`;
    },
    image() {
      return this.variants[this.selectedVariant].image;
    },
    inventory() {
      return this.variants[this.selectedVariant].quantity;
    }
  }
}).$mount('#app');
